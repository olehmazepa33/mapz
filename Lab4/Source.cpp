#include <iostream>
#include <vector>
#include <time.h>
#include <fstream>

using namespace std;

//#define _CRT_SECURE_NO_WARNINGS

//���� �'���� �� ������� ���
class Ball
{
	int x;
	int speed;
public:
	Ball() {};
	Ball(int x, int speed) :x(x), speed(speed) {};
	int GetX() { return x; }
	int GetSpeed() { return speed; }
	void BallPosition(int x) { this->x = x; }
	void Accelerate(int speed) { this->speed = speed; }
	void Copy(Ball  ball)
	{
		x = ball.x;
		speed = ball.speed;
	}
};

class Platform
{
	int width;
	int x;
public:
	Platform() {};
	Platform(int x, int width) :x(x), width(width) {};
	int GetX() { return x; }
	int GetWidth() { return width; }
	void PlatformPosition(int x) { this->x = x; }
	void Expand(int width) { this->width = width; }
	void Copy(Platform  platform)
	{
		x = platform.x;
		width = platform.width;
	}
};

//���� ���, ��� ������ ���� �'����, ��������� �� �����

class Game
{
	Ball  ball;
	Platform platfotm;
	int level;
	Game() {};
public:

	Game(Ball ball, Platform platfotm, int level)
	{
		this->ball.Copy(ball);
		this->platfotm.Copy(platfotm);
		this->level = level;
	}
	Game(int xB, int speed, int xP, int width, int level) :level(level)
	{
		ball.BallPosition(xB);
		ball.Accelerate(speed);

		platfotm.PlatformPosition(xP);
		platfotm.Expand(width);
	}
	void ChangeGame()
	{
		ball.Accelerate(rand() % 10 + 1);
		level = rand() % 10 + 1;
		PrintChanges();
	}
	Ball* GetBall()
	{
		return &ball;
	}
	Platform* GetPlatform()
	{
		return &platfotm;
	}
	void PrintChanges()
	{
		cout << "Game Change: speed=" << ball.GetSpeed() << ", level=" << level << endl;
	}
	// MyMemento Save(string currtime = "");

};

//������ ������, � ��������� ����� ���������� ���

class MyMemento
{
	Ball  ball;
	Platform platform;
	int level;
	string currtime;
public:
	MyMemento(Ball  ball, Platform platform, int level, string crrtime = "")
	{
		this->ball.Copy(ball);
		this->platform.Copy(platform);
		this->level = level;
		if (crrtime == "") {
			time_t seconds = time(NULL);
			tm* currtime;
			currtime = localtime(&seconds);
			this->currtime = asctime(currtime);
		}
		else this->currtime = crrtime;
	}
	Game GetState()
	{
		return Game(ball, platform, level);
	}
	string GetTime()
	{
		return currtime;
	}
	void Info()
	{
		cout << "\t x=" << ball.GetX() << ", speed=" << ball.GetSpeed() << ", level=" << level << endl;
	}
};

/*MyMemento Game::Save(string currtime)
{

	return MyMemento(ball, platfotm, level, currtime);

}*/

//���� �����, ���� ���� ����������� ���
class CareTaker
{
	vector<MyMemento> MyMementos;
	Game* game = NULL;
public:
	CareTaker(Game* game)
	{
		this->game = game;
		ofstream file("data.bin");
		file.clear();
		file.close();

	}
	void BackUp()
	{
		cout << "Game state was backup" << endl;
		//this->MyMementos.push_back(game->Save());

	}
};



//���������� ��������
class Behavior
{
public:
	void virtual SomeUpdate(Game* game) = 0;
};

class ExpandBonus :public Behavior
{
public:
	void SomeUpdate(Game* game)
	{
		Platform* platform = game->GetPlatform();
		platform->Expand(platform->GetWidth() * 2);
		cout << "Expand Bonus started" << endl;
	}
};

class SlowBonus :public Behavior
{
public:
	void SomeUpdate(Game* game)
	{
		Ball* ball = game->GetBall();
		ball->Accelerate(ball->GetSpeed() / 2);
		cout << "Slow Bonus started" << endl;
	}
};
class SpeedBonus :public Behavior
{
public:
	void SomeUpdate(Game* game)
	{
		Ball* ball = game->GetBall();
		ball->Accelerate(ball->GetSpeed() * 2);
		cout << "Slow Bonus started" << endl;
	}
};

//������ ����� ���� ����� �������� ������� �������� ������㳿
class Bonus {
	Behavior* Behav;
public:
	Bonus(Behavior* B)
	{
		Behav = B;
	}
	void SetBehavior(Behavior* B)
	{
		Behav = B;
	}
	void Colision(Game* game)
	{
		Behav->SomeUpdate(game);
	}
};


int main()
{
	//��������� ���
	Game game(4, 4, 1, 6, 1);
	CareTaker CR(&game);

	// 5 ���� ������� �� �������� ���
	for (int i = 0; i < 5; i++)
	{
		CR.BackUp();
		game.ChangeGame();
	}

	cout << endl << endl;

	cout << endl << endl;


	//�������� ��������� ���� ���
	game.PrintChanges();
	//��������� ����� � ��������� ��� �����������
	Bonus bonus(new SlowBonus());
	bonus.Colision(&game);//���������� ��������
	game.PrintChanges();//�������� ���� � ��
	//���������� ��� ����� ��������
	bonus.SetBehavior(new SpeedBonus());
	bonus.Colision(&game);
	game.PrintChanges();
	bonus.SetBehavior(new ExpandBonus());
	bonus.Colision(&game);
	game.PrintChanges();
}