#include <iostream>
#include <list>
#include <time.h>
#include <Windows.h>
using namespace std;
CRITICAL_SECTION cs;

class Bonus {
public:
	virtual void info() = 0;
	virtual ~Bonus() {}
};
class ExpandBonus : public Bonus {
public:
	void info() {
		cout << "expand bonus" << endl;
	}
};

class DivideBonus : public Bonus {
public:
	void info() {
		cout << "divide bonus" << endl;
	}
};

class LaserBonus : public Bonus {
public:
	void info() {
		cout << "laser bonus" << endl;
	}
};

class BonusFactory {
public:
	virtual Bonus* createBonus() = 0;
	virtual ~BonusFactory() {}
};

class ExpandBonusFactory : public BonusFactory {
public:
	Bonus* createBonus() {
		return new ExpandBonus();
	}
};

class DivideBonusFactory : public BonusFactory {
public:
	Bonus* createBonus() {
		return new DivideBonus();
	}
};

class LaserBonusFactory : public BonusFactory {
public:
	Bonus* createBonus() {
		return new LaserBonus();
	}
};

//Singleton
class Music {
public:
	void getMusic() { cout << "Singleton music started" << endl << endl; }
};

class Singleton {
private:
	static Singleton* p_instance;
	Singleton() {}

	Singleton(const Singleton&);
	Singleton& operator=(Singleton&);
public:
	static Singleton* getInstance() {
		EnterCriticalSection(&cs);
		if (!p_instance)
			p_instance = new Singleton();
		LeaveCriticalSection(&cs);
		return p_instance;
	}
	Music music;
};
Singleton* Singleton::p_instance = 0;
//

class Ball {
public:
	void info() {
		cout << "ball created" << endl;
	}
};

class Rocket {
public:
	void info() {
		cout << "rocket created" << endl;
	}
};

class Brick {
private:
	Bonus* bonus;
public:
	Brick() {
		bonus = nullptr;
	}
	Brick(BonusFactory& factory) {
		bonus = factory.createBonus();
	}
	void info() {
		cout << "brick created, bonus: ";
		if (bonus)
			bonus->info();
		else cout << "none" << endl;

	}
};

class Level {
public:
	Ball* ball;
	Rocket* rocket;
	list<Brick*> bricks;
	string difficulty;
	

	void info() {
		ball->info();
		rocket->info();
		for (Brick* b : bricks) {
			b->info();
		}
		cout << "difficulty:" << "easy" << endl;
	}
};

class levelBuilder {
public:
	virtual string getDifficulty() = 0;
};

class Director {
	levelBuilder* builder;
	list<Brick*> getBricks(int bricksCount) {
		list<Brick*> bricks;
		srand(time(0));
		ExpandBonusFactory expand_factory;
		DivideBonusFactory divide_factory;
		LaserBonusFactory laser_factory;
		for (int i = 0; i < bricksCount; i++) {
			if (rand() % 2) bricks.push_back(new Brick());
			else {

				int res = rand() % 3;
				switch (res)
				{
				case 0: bricks.push_back(new Brick(expand_factory)); break;
				case 1: bricks.push_back(new Brick(divide_factory)); break;
				case 2: bricks.push_back(new Brick(laser_factory)); break;
				}
			}
		}
		return bricks;
	}

public:
	void setBuilder(levelBuilder* newBuilder) {
		builder = newBuilder;
	}
	Level* getLevel(int bricksCount) {
		Level* level = new Level();
		level->ball = new Ball();
		level->rocket = new Rocket();
		level->bricks = getBricks(bricksCount);
		level->difficulty = builder->getDifficulty();
		return level;
	}
};

class EasyLevelBuilder : public levelBuilder {
public:
	string getDifficulty() {
		return "easy";
	}
};
class HardLevelBuilder : public levelBuilder {
public:
	string getDifficulty() {
		return "hard";
	}

};

int main(void) {
	InitializeCriticalSection(&cs);
	Singleton::getInstance()->music.getMusic();
	Level* level;
	Director dir;
	EasyLevelBuilder builder;
	dir.setBuilder(&builder);
	level = dir.getLevel(10);
	level->info();
	DeleteCriticalSection(&cs);
}