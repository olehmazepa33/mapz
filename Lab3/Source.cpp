#include <iostream>
#include <string>
#include <vector>
using namespace std;

class Rocket {
	string str;
public:
	Rocket() {
		str = "Rocket bonuses: ";
	}
	virtual string getInfo() {
		return str;
	}
};

class Ball {
public:
	void info() {
		cout << "ball created" << endl;
	}
};

class BonusesDecorator : public Rocket {
public:
	virtual string getInfo() = 0;
};
class Expand : public BonusesDecorator {
	Rocket* r;
public:
	Expand(Rocket* _r) {
		r = _r;
	}
	string getInfo() {
		return r->getInfo() + "Expand, ";
	}
};
class Laser : public BonusesDecorator {
	Rocket* r;
public:
	Laser(Rocket* _r) {
		r = _r;
	}
	string getInfo() {
		return r->getInfo() + "Laser, ";
	}
};
//--------------------------------------------
//��������������
class AbstBrick {
public:
	virtual void print() const = 0;
};

class Brick : public AbstBrick {
public:
	void print() const {
		cout << "Brick created" << endl;
	}
};
class BrickBonus : public AbstBrick {
public:
	void print() const {
		cout << "Brick with Bonus created" << endl;
	}
};
class CompositeBricks : public AbstBrick {
	vector<AbstBrick*> brickList;
public:
	void print() const {
		for (AbstBrick* b : brickList) {
			b->print();
		}
	}
	void add(AbstBrick* b) {
		brickList.push_back(b);
	}
};
class Level {
	Rocket* rocket;
	Ball* ball;
public:
	Level(int lvlNum) {
		cout << "Level " << lvlNum << " created" << endl;
		//------------------------------
		//���������� ������ �� Brick
		auto_ptr<CompositeBricks> bricks(new CompositeBricks());
		auto_ptr<CompositeBricks> bricks1(new CompositeBricks());
		auto_ptr<CompositeBricks> bricks2(new CompositeBricks());

		auto_ptr<Brick> brick1(new Brick());
		auto_ptr<Brick> brick2(new Brick());
		auto_ptr<Brick> brick3(new Brick());
		bricks1->add(brick1.get());
		bricks1->add(brick2.get());
		bricks1->add(brick3.get());
		auto_ptr<BrickBonus> brickB1(new BrickBonus());
		auto_ptr<BrickBonus> brickB2(new BrickBonus());
		bricks2->add(brickB1.get());
		bricks2->add(brickB2.get());

		bricks->add(bricks1.get());
		bricks->add(bricks2.get());
		bricks->print();
		//------------------------------
		rocket = new Rocket();
		ball = new Ball();
	}

	Rocket* getRocket() {
		return rocket;
	}
};
//--------------------------------------------
//�����
class gameFacade {
private:
	Level* lvl;
	int lvlNum;
public:
	void selectLevel(int _lvlNum) {
		lvlNum = _lvlNum;
	}
	void startGame() {
		cout << "game started" << endl << endl;
		lvl = new Level(lvlNum);
		Rocket* r = lvl->getRocket();
		cout << r->getInfo() << endl;
		r = new Expand(r);
		r = new Laser(r);
		cout << r->getInfo() << endl;
	}
};
//--------------------------------------------
int main()
{
	gameFacade game = gameFacade();
	game.selectLevel(2);
	game.startGame();

	system("pause");
	return 0;
}