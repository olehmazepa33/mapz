﻿namespace Interpretator
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainWindow));
            this.TextBox = new System.Windows.Forms.TextBox();
            this.MagicButton = new System.Windows.Forms.Button();
            this.Console = new System.Windows.Forms.RichTextBox();
            this.NotMagicButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // TextBox
            // 
            this.TextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.TextBox.BackColor = System.Drawing.SystemColors.Info;
            this.TextBox.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.TextBox.ForeColor = System.Drawing.Color.DarkGreen;
            this.TextBox.Location = new System.Drawing.Point(16, 75);
            this.TextBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.TextBox.Multiline = true;
            this.TextBox.Name = "TextBox";
            this.TextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.TextBox.Size = new System.Drawing.Size(703, 485);
            this.TextBox.TabIndex = 0;
            // 
            // MagicButton
            // 
            this.MagicButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.MagicButton.AutoSize = true;
            this.MagicButton.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.MagicButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.MagicButton.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.MagicButton.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.MagicButton.Location = new System.Drawing.Point(830, 12);
            this.MagicButton.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.MagicButton.Name = "MagicButton";
            this.MagicButton.Size = new System.Drawing.Size(159, 55);
            this.MagicButton.TabIndex = 1;
            this.MagicButton.Text = "Обчислити";
            this.MagicButton.UseVisualStyleBackColor = false;
            this.MagicButton.Click += new System.EventHandler(this.MagicButton_Click);
            // 
            // Console
            // 
            this.Console.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Console.BackColor = System.Drawing.SystemColors.HighlightText;
            this.Console.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Console.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Console.Location = new System.Drawing.Point(727, 75);
            this.Console.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Console.Name = "Console";
            this.Console.Size = new System.Drawing.Size(803, 485);
            this.Console.TabIndex = 2;
            this.Console.Text = "";
            this.Console.TextChanged += new System.EventHandler(this.Console_TextChanged);
            // 
            // NotMagicButton
            // 
            this.NotMagicButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.NotMagicButton.AutoSize = true;
            this.NotMagicButton.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.NotMagicButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.NotMagicButton.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.NotMagicButton.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.NotMagicButton.Location = new System.Drawing.Point(1131, 15);
            this.NotMagicButton.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.NotMagicButton.Name = "NotMagicButton";
            this.NotMagicButton.Size = new System.Drawing.Size(303, 55);
            this.NotMagicButton.TabIndex = 3;
            this.NotMagicButton.Text = "Вивести токени і дерево";
            this.NotMagicButton.UseVisualStyleBackColor = false;
            this.NotMagicButton.Click += new System.EventHandler(this.NotMagicButton_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Gray;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label1.Location = new System.Drawing.Point(175, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(292, 39);
            this.label1.TabIndex = 4;
            this.label1.Text = "Введіть ваш код";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(1543, 573);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.NotMagicButton);
            this.Controls.Add(this.Console);
            this.Controls.Add(this.MagicButton);
            this.Controls.Add(this.TextBox);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "MainWindow";
            this.Text = "Інтерпретатор";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox TextBox;
        private System.Windows.Forms.Button MagicButton;
        private System.Windows.Forms.RichTextBox Console;
		private System.Windows.Forms.Button NotMagicButton;
        private System.Windows.Forms.Label label1;
    }
}

