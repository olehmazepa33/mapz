bool b
b = TRUE
b = FALSE
print(b)

int i
i = {2 + 5}
while ({i > 0})
  i = {i - 1}
  print(i)
end

str url
url = "https://google.com.ua"
b = visit(url)
if (b)
  str pattern
  pattern = "script"
  i = count(url, pattern)
  print("Found tags: ")
  print(i)

  print("Here is your first tag: ")
  print(tag(url, pattern, 0))
end