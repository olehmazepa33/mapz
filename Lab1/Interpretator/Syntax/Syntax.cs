﻿//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
//------------------------------------------------------------------------------
namespace Interpretator
{
    //--------------------------------------------------------------------------
    //Містить усі змінні
    //--------------------------------------------------------------------------
    public class Context
    {
        public Dictionary<string, object> Variables = new Dictionary<string, object>();
    }
    //--------------------------------------------------------------------------
    //виконує заданий вираз
    //--------------------------------------------------------------------------
    public interface IExpression
    {
        object Execute(Context context);
    }
    //--------------------------------------------------------------------------
    //Створюємо вираз
    //--------------------------------------------------------------------------
    public interface IExpressionCreator
    {
        IExpression CreateExpression(ref List<Token> tokens, ref int position);
    }
    //--------------------------------------------------------------------------
    //Синтаксичний аналізатор
    //--------------------------------------------------------------------------
    public class SyntaxAnalyzer
    {
        public static string AnalyzerOutput = "";           //до консолі
        public int Position = 0;                            //теперішня позиція
        private List<Token> _tokens = null;                 //список токенів

        public ReadOnlyCollection<Token> InputTokens => new ReadOnlyCollection<Token>(_tokens); 

        private List<IExpressionCreator> _expressionPattern;//список виразів
        //----------------------------------------------------------------------
        public SyntaxAnalyzer()
        {
            _expressionPattern = new List<IExpressionCreator>
            {
                new AssignmenCreator(this),
                new ConstIntCreator(),
                new ConstStringCreator(),
                new TrueFalseExpressionCreator(),
                new PlusExpressionCreator(this),
                new MinusExpressionCreator(this),
                new ComparisonCreator(this),
                new VariableExpressionCreator(),
                new PrintFunctionCreator(this),
                new IntDeclarationCreator(),
                new BoolDeclarationCreator(),
                new IfElseExpressionCreator(this),
                new WhileExpressionsCreator(this),
                new DoWhileExpressionCreator(this),
                new StringDeclarationCreator(),
                new VisitFunctionCreator(this),
                new TagExpressionCreator(this),
                new CountFunctionCreator(this),
                new ToFileCreator(this),
                new IntToStringCreator(this)
            };
        }
        //----------------------------------------------------------------------
        //Будуємо дерево 
        //----------------------------------------------------------------------
        public IExpression TreeBuilder(List<Token> tokens)
        {
            int position = 0;
            IList<IExpression> expressions = new List<IExpression>();//будує дерево
            while (position < tokens.Count)
            {
                var current = GetCurrentExpression(tokens, ref position);
                expressions.Add(current);
            }
            return new SequanceExpression(expressions);
        }
        //----------------------------------------------------------------------
        //перевірка чи шаблон існує
        //----------------------------------------------------------------------
        public IExpression GetCurrentExpression(List<Token> tokens, ref int position)
        {
            foreach (var expressionCreator in _expressionPattern)
            {
                var result = expressionCreator.CreateExpression(ref tokens, ref position);
                if (result != null)
                    return result;
            }
            throw new Exception($"No such pattern starting at {tokens[position]}");
        }
        //----------------------------------------------------------------------
        //послідовність виконання виразів
        //----------------------------------------------------------------------
        public class SequanceExpression : IExpression
        {
            private readonly List<IExpression> _expressionList;
			public readonly IReadOnlyList<IExpression> ExpressionList;
            //------------------------------------------------------------------
            public SequanceExpression(IList<IExpression> expressions)
            {
                _expressionList = new List<IExpression>(expressions);
				ExpressionList = _expressionList;
            }
            //------------------------------------------------------------------
            public object Execute(Context context)
            {
                foreach (var expression in _expressionList)
                {
                    expression.Execute(context);
                }
                return null;
            }
        }
    }
}
//------------------------------------------------------------------------------