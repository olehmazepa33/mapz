﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Interpretator
{
	public class TreePrinter
	{
		public string PrintTree(IExpression expression)
		{
			StringBuilder builder = new StringBuilder();
			PrintTreeRecursively(ref builder, 0, expression);
			return builder.ToString();
		}

		private void PrintTreeRecursively(ref StringBuilder builder, int offset, IExpression expression)
		{
			switch (expression)
			{
				case SyntaxAnalyzer.SequanceExpression seqExpression:
				{
					foreach (IExpression child in seqExpression.ExpressionList)
					{
						PrintTreeRecursively(ref builder, offset, child);
						builder.AppendLine();
					}
				}
				break;

				case PlusExpression plusExpression:
				{
					AppendSpaces(ref builder, offset);
					builder.AppendLine("PLUS");
					PrintTreeRecursively(ref builder, offset + 1, plusExpression.LeftOperand);
					PrintTreeRecursively(ref builder, offset + 1, plusExpression.RightOperand);
				}
				break;

				case MinusExpression minusExpression:
				{
					AppendSpaces(ref builder, offset);
					builder.AppendLine("MINUS");
					PrintTreeRecursively(ref builder, offset + 1, minusExpression.LeftOperand);
					PrintTreeRecursively(ref builder, offset + 1, minusExpression.RightOperand);
				}
				break;

				case AssignmentExpression assignExpression:
				{
					AppendSpaces(ref builder, offset);
					builder.AppendLine("ASSIGN");
					PrintTreeRecursively(ref builder, offset + 1, new VariableExpression(assignExpression.LeftOperand));
					PrintTreeRecursively(ref builder, offset + 1, assignExpression.RightOperand);
				}
				break;

				case IntDeclarationExpression intDeclExpression:
				{
					AppendSpaces(ref builder, offset);
					builder.AppendLine("DECLARE-INT");
					PrintTreeRecursively(ref builder, offset + 1, new ConstStringExpression(intDeclExpression.InputData));
				}
				break;

				case BoolDeclarationExpression boolDeclExpression:
				{
					AppendSpaces(ref builder, offset);
					builder.AppendLine("DECLARE-BOOL");
					PrintTreeRecursively(ref builder, offset + 1, new ConstStringExpression(boolDeclExpression.InputData));
				}
				break;

				case StringDeclarationExpression strDeclExpression:
				{
					AppendSpaces(ref builder, offset);
					builder.AppendLine("DECLARE-STRING");
					PrintTreeRecursively(ref builder, offset + 1, new ConstStringExpression(strDeclExpression.InputData));
				}
				break;

				case ConstIntExpression constIntExpression:
				{
					AppendSpaces(ref builder, offset);
					builder.AppendLine(constIntExpression.Value.ToString());
				}
				break;

				case TrueFalseExpression constBoolExpression:
				{
					AppendSpaces(ref builder, offset);
					builder.AppendLine(constBoolExpression.Value.ToString());
				}
				break;

				case ConstStringExpression constStrExpression:
				{
					AppendSpaces(ref builder, offset);
					builder.AppendLine(String.Format("\"{0}\"", constStrExpression.Value));
				}
				break;

				case IfElseExpression ifElseExpression:
				{
					AppendSpaces(ref builder, offset);
					if (ifElseExpression.FalseExpression != null)
					{
						builder.AppendLine(String.Format("IF-ELSE"));
						PrintTreeRecursively(ref builder, offset + 1, ifElseExpression.Condition);
						PrintTreeRecursively(ref builder, offset + 1, ifElseExpression.TrueExpression);
						PrintTreeRecursively(ref builder, offset + 1, ifElseExpression.FalseExpression);
					}
					else
					{
						builder.AppendLine(String.Format("IF"));
						PrintTreeRecursively(ref builder, offset + 1, ifElseExpression.Condition);
						PrintTreeRecursively(ref builder, offset + 1, ifElseExpression.TrueExpression);
					}
				}
				break;

				case ComparisonExpression comparisionExpression:
				{
					Dictionary<string, string> signToName = new Dictionary<string, string>()
					{
						{ "==", "EQUAL" },
						{ "!=", "NOT-EQUAL" },
						{ ">", "MORE" },
						{ "<", "LESS" },
						{ ">=", "MORE-OR-EQUAL" },
						{ "<=", "LESS-OR-EQUAL" }
					};
					AppendSpaces(ref builder, offset);
					builder.AppendLine(signToName[comparisionExpression.Sign]);
					PrintTreeRecursively(ref builder, offset + 1, comparisionExpression.LeftOperand);
					PrintTreeRecursively(ref builder, offset + 1, comparisionExpression.RightOperand);
				}
				break;

				case WhileExpression whileExpression:
				{
					AppendSpaces(ref builder, offset);
					builder.AppendLine(String.Format("WHILE-LOOP"));
					PrintTreeRecursively(ref builder, offset + 1, whileExpression.Condition);
					PrintTreeRecursively(ref builder, offset + 1, whileExpression.TrueExpression);
				}
				break;

				case DoWhileExpression whileExpression:
				{
					AppendSpaces(ref builder, offset);
					builder.AppendLine(String.Format("DO-WHILE-LOOP"));
					PrintTreeRecursively(ref builder, offset + 1, whileExpression.Condition);
					PrintTreeRecursively(ref builder, offset + 1, whileExpression.TrueExpression);
				}
				break;

				case VariableExpression varExpression:
				{
					AppendSpaces(ref builder, offset);
					builder.AppendLine(varExpression.VariableName);
				}
				break;

				case TagExpression tagExpression:
				{
					AppendSpaces(ref builder, offset);
					builder.AppendLine(String.Format("TAG"));
					PrintTreeRecursively(ref builder, offset + 1, tagExpression.Url);
					PrintTreeRecursively(ref builder, offset + 1, tagExpression.TagName);
					PrintTreeRecursively(ref builder, offset + 1, tagExpression.TagNum);
				}
				break;

				case PrintFunction printFunction:
				{
					AppendSpaces(ref builder, offset);
					builder.AppendLine(String.Format("PRINT"));
					PrintTreeRecursively(ref builder, offset + 1, printFunction.InputData);
				}
				break;

				case CountFunction countFunction:
				{
					AppendSpaces(ref builder, offset);
					builder.AppendLine(String.Format("COUNT"));
					PrintTreeRecursively(ref builder, offset + 1, countFunction.Url);
					PrintTreeRecursively(ref builder, offset + 1, countFunction.TagName);
				}
				break;

				case IntToStringFunction i2sFunction:
				{
					AppendSpaces(ref builder, offset);
					builder.AppendLine(String.Format("INT_TO_STRING"));
					PrintTreeRecursively(ref builder, offset + 1, i2sFunction.Value);
				}
				break;

				case ToFileFunction toFileFunction:
				{
					AppendSpaces(ref builder, offset);
					builder.AppendLine(String.Format("TO_FILE"));
					PrintTreeRecursively(ref builder, offset + 1, toFileFunction.Url);
				}
				break;

				case VisitFunction visitFunction:
				{
					AppendSpaces(ref builder, offset);
					builder.AppendLine(String.Format("VISIT"));
					PrintTreeRecursively(ref builder, offset + 1, visitFunction.Url);
				}
				break;

				default:
				{
					AppendSpaces(ref builder, offset);
					builder.AppendLine("UNKNOWN");
				}
				break;
			}
		}

		void AppendSpaces(ref StringBuilder builder, int count)
		{
			for (int i = 0; i < count; ++i)
			{
				builder.Append("    ");
			}
		}
	}
}
