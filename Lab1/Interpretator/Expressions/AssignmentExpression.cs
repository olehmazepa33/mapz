﻿//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
//------------------------------------------------------------------------------
namespace Interpretator
{
    //--------------------------------------------------------------------------
    //assigns right operand to the left operand
    //--------------------------------------------------------------------------
    class AssignmentExpression : IExpression
    {
        public string LeftOperand { get; private set; }
		public IExpression RightOperand { get; private set; }
		//----------------------------------------------------------------------
		public object Execute(Context context)
        {
            object value;
            //checking if left operand exists in the dictionary
            if (context.Variables.ContainsKey(LeftOperand))
            {
                value = RightOperand.Execute(context);
                //cheking if both operands are the same type
                if (context.Variables[LeftOperand].GetType() == value.GetType())
                    context.Variables[LeftOperand] = value;
                else throw new Exception($"Error! Different types of operands!");
            }
            else throw new Exception($"Variable {LeftOperand} do not exist!"); 
            
            return value;
        }
        //----------------------------------------------------------------------
        public AssignmentExpression(string left, IExpression right)
        {
            RightOperand = right;
            LeftOperand = left;
        }
    }
    //--------------------------------------------------------------------------
    //
    //--------------------------------------------------------------------------
    class AssignmenCreator : IExpressionCreator
    {
        private readonly SyntaxAnalyzer _analyzer;
        //----------------------------------------------------------------------
        public AssignmenCreator(SyntaxAnalyzer analyzer)
        {
            _analyzer = analyzer;
        }
        //----------------------------------------------------------------------
        public IExpression CreateExpression(ref List<Token> tokens, ref int position)
        {
            var leftOperand = tokens[position];
            //checking tokens values
            if (leftOperand.getType() != TokenType.VariableName || tokens[position + 1].getType() != TokenType.Assign)
                return null;
            else
            {
                position+=2;
                var rightOperand = _analyzer.GetCurrentExpression(tokens, ref position);
                return new AssignmentExpression(leftOperand.GetLexem(), rightOperand);
            }
        }
    }
}
//------------------------------------------------------------------------------