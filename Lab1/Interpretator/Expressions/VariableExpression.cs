﻿//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Diagnostics;
//------------------------------------------------------------------------------
namespace Interpretator
{
    //--------------------------------------------------------------------------
    //class to vork work with variables
    //--------------------------------------------------------------------------
    class VariableExpression : IExpression
    {
        public string VariableName { get; private set; }
		//----------------------------------------------------------------------
		public object Execute(Context context)
        {
            if (context.Variables.ContainsKey(VariableName))
            {
                Debug.WriteLine(context.Variables[VariableName]);
                return context.Variables[VariableName];
            }
            else throw new Exception($"Variable {VariableName} do not exist!");
        }
        //----------------------------------------------------------------------
        public VariableExpression(string variable)
        {
            VariableName = variable;
        }
    }
    //--------------------------------------------------------------------------
    class VariableExpressionCreator : IExpressionCreator
    {
        public IExpression CreateExpression(ref List<Token> tokens, ref int position)
        {
            if (tokens[position].getType() != TokenType.VariableName)
                return null;
            else
            {
                return new VariableExpression(tokens[position++].GetLexem());
            }
        }
    }
}
//------------------------------------------------------------------------------