﻿//------------------------------------------------------------------------------
using System.Collections.Generic;
//------------------------------------------------------------------------------
namespace Interpretator
{
    //--------------------------------------------------------------------------
    class TrueFalseExpression : IExpression
    {
        public bool Value { get; private set; }
        //----------------------------------------------------------------------
        public object Execute(Context context)
        {
            return Value;
        }
        //----------------------------------------------------------------------
        public TrueFalseExpression(bool value)
        {
            Value = value;
        }

		public override string ToString()
		{
			return Value.ToString();
		}
	}
    //--------------------------------------------------------------------------
    class TrueFalseExpressionCreator :IExpressionCreator
    {
        //----------------------------------------------------------------------
        public IExpression CreateExpression(ref List<Token> tokens, ref int position)
        {
            if (tokens[position].getType() != TokenType.True && tokens[position].getType() != TokenType.False)
                return null;
            else
            {
                if (tokens[position].GetLexem() == "TRUE")
                {
                    position++;
                    return new TrueFalseExpression(true);
                }
                else
                {
                    position++;
                    return new TrueFalseExpression(false);
                }
            }
        }
    }
}
//------------------------------------------------------------------------------