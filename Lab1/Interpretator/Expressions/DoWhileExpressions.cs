﻿//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
//------------------------------------------------------------------------------
namespace Interpretator
{
    class DoWhileExpression : IExpression
    {
        public IExpression Condition { get; private set; }
		public IExpression TrueExpression { get; private set; }
		//----------------------------------------------------------------------
		public DoWhileExpression(IExpression condition, IExpression trueExpression)
        {
            Condition = condition;
            TrueExpression = trueExpression;
        }
        //----------------------------------------------------------------------
        public object Execute(Context context)
        {
            //execute the context
            do
            {
                TrueExpression.Execute(context);
            } while ((bool)Condition.Execute(context));

            return null;
        }
    }
    //--------------------------------------------------------------------------
    class DoWhileExpressionCreator : IExpressionCreator
    {
        private SyntaxAnalyzer _analyzer;
        //----------------------------------------------------------------------
        public DoWhileExpressionCreator(SyntaxAnalyzer analyzer)
        {
            _analyzer = analyzer;
        }
        //----------------------------------------------------------------------
        public IExpression CreateExpression(ref List<Token> tokens, ref int position)
        {
            //checking if the token is "while"
            if (tokens[position].getType() != TokenType.Do)
                return null;


            //adding all the tokens to the tree
            position++;
            var tokensList = new List<Token>();
            while (tokens[position].getType() != TokenType.While)
            {
                tokensList.Add(tokens[position]);
                position++;
            }
            var trueExpression = new SyntaxAnalyzer().TreeBuilder(tokensList);
            //checking if there is "end"
            if (tokens[position].getType() != TokenType.While)
                throw new Exception($"While statement syntax error: keyword 'while' is missing!");

            if (tokens[++position].getType() != TokenType.LeftParenthesis)
                throw new Exception($"While statement syntax error: '(' is missing!");

            position++;
            var condition = _analyzer.GetCurrentExpression(tokens, ref position);

            if (tokens[position].getType() != TokenType.RightParenthesis)
                throw new Exception($"While statement syntax error: ')' is missing!");

            position++;
            return new DoWhileExpression(condition, trueExpression);

        }
    }
}
//------------------------------------------------------------------------------