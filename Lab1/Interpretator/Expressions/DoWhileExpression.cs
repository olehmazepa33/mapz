﻿//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
//------------------------------------------------------------------------------
namespace Interpretator
{
    class DoWhileExpression : IExpression
    {
        private IExpression _condition;
        private IExpression _trueExpression;
        //------------------------------------------------------------------------------
        public DoWhileExpression(IExpression condition, IExpression trueExpression)
        {
            _condition = condition;
            _trueExpression = trueExpression;
        }
        //------------------------------------------------------------------------------
        public object Execute(Context context)
        {
            //execute the context
            while ((bool)_condition.Execute(context))
            {
                _trueExpression.Execute(context);
            }
            return null;
        }
    }
    //------------------------------------------------------------------------------
    class DoWhileExpressionCreator : IExpressionCreator
    {
        private SyntaxAnalyzer _analyzer;
        //------------------------------------------------------------------------------
        public DoWhileExpressionCreator(SyntaxAnalyzer analyzer)
        {
            _analyzer = analyzer;
        }
        //------------------------------------------------------------------------------
        public IExpression CreateExpression(ref List<Token> tokens, ref int position)
        {
            //checking if the token is "while"
            //if (tokens[position].getType() != TokenType.DoWhile)
                return null;

            if (tokens[++position].getType() != TokenType.LeftParenthesis)
                throw new Exception($"While statement syntax error: '(' is missing!");

            position++;
            var condition = _analyzer.GetCurrentExpression(tokens, ref position);

            if (tokens[position].getType() != TokenType.RightParenthesis)
                throw new Exception($"While statement syntax error: ')' is missing!");
            //adding all the tokens to the tree
            position++;
            var tokensList = new List<Token>();
            while (tokens[position].getType() != TokenType.End)
            {
                tokensList.Add(tokens[position]);
                position++;
            }
            var trueExpression = new SyntaxAnalyzer().TreeBuilder(tokensList);
            //checking if there is "end"
            if (tokens[position].getType() != TokenType.End)
                throw new Exception($"While statement syntax error: keyword 'end' is missing!");
            else
            {
                position++;
                return new DoWhileExpression(condition, trueExpression);
            }
        }
    }
}
//------------------------------------------------------------------------------