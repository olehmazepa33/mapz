﻿//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
//------------------------------------------------------------------------------
namespace Interpretator
{
    //--------------------------------------------------------------------------
    class MinusExpression : IExpression
    {
        public IExpression LeftOperand { get; private set; }
        public IExpression RightOperand { get; private set; }
		//----------------------------------------------------------------------
		public object Execute(Context context)
        {
            var left = LeftOperand.Execute(context);
            var right = RightOperand.Execute(context);
            if (LeftOperand is ConstIntExpression)
            {
                if ((LeftOperand is ConstIntExpression) || ((RightOperand is VariableExpression) && right.GetType() == left.GetType()))
                    return (object)((dynamic)left - (dynamic)right);
            }
            else if (LeftOperand is VariableExpression)
            {
                if ((RightOperand is ConstIntExpression) && right.GetType() == left.GetType() || ((RightOperand is VariableExpression) && right.GetType() != typeof(String)))
                    return (object)((dynamic)left - (dynamic)right);
            }
            throw new Exception($"MinusException: this operation available only for numbers!"); ;
        }

        public MinusExpression(IExpression left, IExpression right)
        {
            LeftOperand = left;
            RightOperand = right;
        }

		public override string ToString()
		{
			return String.Format("{{{0} - {1}}}", LeftOperand, RightOperand);
		}
	}
    //------------------------------------------------------------------------------
    class MinusExpressionCreator : IExpressionCreator
    {
        private SyntaxAnalyzer _analyzer;
        //------------------------------------------------------------------------------
        public MinusExpressionCreator(SyntaxAnalyzer analyzer)
        {
            _analyzer = analyzer;
        }
        //------------------------------------------------------------------------------
        public IExpression CreateExpression(ref List<Token> tokens, ref int position)
        {
            var startPosition = position;
            //checking if there are brackets arount the expression
            if (tokens[position].getType() != TokenType.LeftBracket)
                return null;

            position++;
            var leftOperand = _analyzer.GetCurrentExpression(tokens, ref position);

            if (tokens[position].getType() != TokenType.Minus)
            {
                position = startPosition;
                return null;
            }

            position++;
            var rightOPerand = _analyzer.GetCurrentExpression(tokens, ref position);

            if (tokens[position].getType() == TokenType.RightBracket)
            {
                position++;
                return new MinusExpression(leftOperand, rightOPerand);
            }
            else
            {
                position = startPosition;
                throw new Exception("SyntaxErrorException in 'MINUS' declaration!");
            }
        }
    }
}
//------------------------------------------------------------------------------