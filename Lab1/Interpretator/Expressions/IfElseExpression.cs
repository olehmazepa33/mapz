﻿//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
//------------------------------------------------------------------------------
namespace Interpretator
{
    //--------------------------------------------------------------------------
    class IfElseExpression : IExpression
    {
        public IExpression Condition { get; private set; }
		public IExpression TrueExpression { get; private set; }
		public IExpression FalseExpression { get; private set; }
		//----------------------------------------------------------------------
		public object Execute(Context context)
        {
            bool condition = Convert.ToBoolean(Condition.Execute(context));

            if (condition) return TrueExpression.Execute(context);
            else
            {
                if (FalseExpression == null)
                    return null;
                else return FalseExpression.Execute(context);
            }
        }
        //----------------------------------------------------------------------
        public IfElseExpression(IExpression condition, IExpression trueExpression, IExpression falseExpression)
        {
            TrueExpression = trueExpression;
            Condition = condition;
            FalseExpression = falseExpression;
        }
    }
    //--------------------------------------------------------------------------
    class IfElseExpressionCreator : IExpressionCreator
    {
        private SyntaxAnalyzer _analyzer;
        //----------------------------------------------------------------------
        public IfElseExpressionCreator(SyntaxAnalyzer analyzer)
        {
            _analyzer = analyzer;
        }
        //----------------------------------------------------------------------
        public IExpression CreateExpression(ref List<Token> tokens, ref int position)
        {
            //ckecking if the token == "if"
            if (tokens[position].getType() != TokenType.If)
                return null;
            //checking if there is "(" after "if"
            if (tokens[++position].getType() != TokenType.LeftParenthesis)
                throw new Exception($"If statement syntax error: '(' is missing!");
            //analyzing the expression
            position++;
            var condition = _analyzer.GetCurrentExpression(tokens, ref position);
            //checking if there is ")" after "if"
            if (tokens[position].getType() != TokenType.RightParenthesis)
                throw new Exception($"If statement syntax error: ')' is missing!");
            //adding all the tokens to the tree
            position++;
            var tokensList = new List<Token>();
            while (tokens[position].getType() != TokenType.Else && tokens[position].getType() != TokenType.End)
            {
                tokensList.Add(tokens[position]);
                position++;
            }
            //building the tree
            var trueExpression = new SyntaxAnalyzer().TreeBuilder(tokensList);
            //if we've come to an end executing the "true" expression
            if (tokens[position].getType() == TokenType.End)
            {
                position++;
                return new IfElseExpression(condition, trueExpression, null);
            }
            else if (tokens[position].getType() != TokenType.Else)
                throw new Exception($"If statement syntax error: 'end' is missing!");
            
            position++;
            tokensList = new List<Token>();
            while (tokens[position].getType() != TokenType.End)
            {
                tokensList.Add(tokens[position]);
                position++;
            }
            var falseExpression = new SyntaxAnalyzer().TreeBuilder(tokensList);

            if (tokens[position].getType() != TokenType.End)
                throw new Exception($"If statement syntax error: keyword 'end' is missing!");
            else
            {
                position++;
                return new IfElseExpression(condition, trueExpression, falseExpression);
            }
        }
    }
}
//------------------------------------------------------------------------------