﻿//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
//------------------------------------------------------------------------------
namespace Interpretator
{
    class PlusExpression : IExpression
    {
        public IExpression LeftOperand { get; private set; }
        public IExpression RightOperand { get; private set; }
		//----------------------------------------------------------------------
		public object Execute(Context context)
        {
            var left = LeftOperand.Execute(context);
            var right = RightOperand.Execute(context);
            
            if (left.GetType() != right.GetType())
            {
                throw new Exception($"PlusException: Different variables types: '{left.GetType()}' and '{right.GetType()}'!");
            }
            else return (object) ((dynamic) left + (dynamic) right);
        }
        //----------------------------------------------------------------------
        public PlusExpression(IExpression left, IExpression right)
        {
            LeftOperand = left;
            RightOperand = right;
        }
    }
    //--------------------------------------------------------------------------
    class PlusExpressionCreator : IExpressionCreator
    {
        private SyntaxAnalyzer _analyzer;
        //----------------------------------------------------------------------
        public PlusExpressionCreator(SyntaxAnalyzer analyzer)
        {
            _analyzer = analyzer;
        }
        //----------------------------------------------------------------------
        public IExpression CreateExpression(ref List<Token> tokens, ref int position)
        {
            var startPosition = position;
            if (tokens[position].getType() != TokenType.LeftBracket)
                return null;

            position++;
            var leftOperand = _analyzer.GetCurrentExpression(tokens, ref position);

            if (tokens[position].getType() != TokenType.Plus)
            {
                position = startPosition;
                return null;
            }

            position++;
            var rightOPerand = _analyzer.GetCurrentExpression(tokens, ref position);

            if (tokens[position].getType() == TokenType.RightBracket)
            {
                position++;
                return new PlusExpression(leftOperand, rightOPerand);
            }
            else
            {
                position = startPosition;
                throw new Exception("SyntaxErrorException in 'PLUS' declaration!");
            }
        }
    }
}
//------------------------------------------------------------------------------