﻿//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
//------------------------------------------------------------------------------
namespace Interpretator
{
    //--------------------------------------------------------------------------
    //value of the int expression
    //--------------------------------------------------------------------------
    class ConstIntExpression : IExpression
    {
        public int Value { get; private set; }

		public ConstIntExpression(int value)
        {
            Value = value;
        }
        public object Execute(Context context)
        {
            return Value;
        }
    }
    //--------------------------------------------------------------------------
    public class ConstIntCreator : IExpressionCreator
    {
        public IExpression CreateExpression(ref List<Token> tokens, ref int position)
        {
            if (tokens[position].getType() != TokenType.NumberConst)
                return null;
            else
                return new ConstIntExpression(Convert.ToInt32(tokens[position++].GetLexem()));
        }
    }
}
//------------------------------------------------------------------------------