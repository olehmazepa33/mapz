﻿//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
//------------------------------------------------------------------------------
namespace Interpretator
{
    //--------------------------------------------------------------------------
    class StringDeclarationExpression : IExpression
    {
        public string InputData { get; private set; }

        public object Execute(Context context)
        {
            context.Variables.Add(InputData, "");
            return null;
        }

        public StringDeclarationExpression(string inputExpression)
        {
            InputData = inputExpression;
        }
	}
    //--------------------------------------------------------------------------
    class StringDeclarationCreator : IExpressionCreator
    {
        public IExpression CreateExpression(ref List<Token> tokens, ref int position)
        {
            if (tokens[position].getType() != TokenType.String)
                return null;
            else
            {
                var variable = tokens[++position];
                if (variable.getType() == TokenType.VariableName)
                {
                    position++;
                    return new StringDeclarationExpression(variable.GetLexem());
                }
                else throw new Exception("String Declaration Error!");
            }
        }
    }
}
//------------------------------------------------------------------------------