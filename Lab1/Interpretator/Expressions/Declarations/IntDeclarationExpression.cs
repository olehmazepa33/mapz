﻿//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
//------------------------------------------------------------------------------
namespace Interpretator
{
    //--------------------------------------------------------------------------
    //Використовується для оголошення цілих чисел
    //--------------------------------------------------------------------------
    class IntDeclarationExpression : IExpression
    {
        public string InputData { get; private set; }
		//----------------------------------------------------------------------
		public object Execute(Context context)
        {
            context.Variables.Add(InputData, 0);
            return null;
        }
        //----------------------------------------------------------------------
        public IntDeclarationExpression(string inputExpression)
        {
            InputData = inputExpression;
        }
    }
    //--------------------------------------------------------------------------
    class IntDeclarationCreator : IExpressionCreator
    {
        public IExpression CreateExpression(ref List<Token> tokens, ref int position)
        {
            //Перевірка типу лексем
            if (tokens[position].getType() != TokenType.Int)
                return null;
            else
            {
                var variable = tokens[++position];      //назва змінної
                if (variable.getType() == TokenType.VariableName)
                {
                    position++;
                    //створення змінної int із заданим іменем
                    return new IntDeclarationExpression(variable.GetLexem());
                }
                else throw new Exception("Int Declaration Error!");
            }
        }
    }
}
//------------------------------------------------------------------------------