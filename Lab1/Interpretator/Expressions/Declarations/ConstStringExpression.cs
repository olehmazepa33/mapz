﻿//------------------------------------------------------------------------------
using System.Collections.Generic;
//------------------------------------------------------------------------------
namespace Interpretator
{
    //--------------------------------------------------------------------------
    class ConstStringExpression : IExpression
    {
        public string Value { get; private set; }
		//----------------------------------------------------------------------
		public ConstStringExpression(string value)
        {
            Value = value;
        }
        //----------------------------------------------------------------------
        public object Execute(Context context)
        {
            return Value;
        }
    }
    //--------------------------------------------------------------------------
    public class ConstStringCreator : IExpressionCreator
    {
        //----------------------------------------------------------------------
        public IExpression CreateExpression(ref List<Token> tokens, ref int position)
        {
            if (tokens[position].getType() != TokenType.StringConst)
                return null;
            else
                return new ConstStringExpression(tokens[position++].GetLexem().Trim('"'));
        }
    }
}
//------------------------------------------------------------------------------