﻿//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
//------------------------------------------------------------------------------
namespace Interpretator
{
    //--------------------------------------------------------------------------
    class BoolDeclarationExpression : IExpression
    {
        public string InputData { get; private set; }

		public BoolDeclarationExpression(string inputData)
        {
            InputData = inputData;
        }
        public object Execute(Context context)
        {
            context.Variables.Add(InputData, false);
            return null;
        }
    }
    //--------------------------------------------------------------------------
    class BoolDeclarationCreator : IExpressionCreator
    {
        //----------------------------------------------------------------------
        public IExpression CreateExpression(ref List<Token> tokens, ref int position)
        {
            if (tokens[position].getType() != TokenType.Bool)
                return null;
            else
            {
                var variable = tokens[++position];
                if (variable.getType() == TokenType.VariableName)
                {
                    position++;
                    return new BoolDeclarationExpression(variable.GetLexem());
                }
                else throw new Exception("Bool Declaration Error!");
            }
        }
    }
}
//------------------------------------------------------------------------------