﻿//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Net;
using xNet;
using AngleSharp.Parser.Html;
//------------------------------------------------------------------------------
namespace Interpretator
{
    class TagExpression : IExpression
    {
        public IExpression Url { get; private set; }
        public IExpression TagName { get; private set; }
		public IExpression TagNum { get; private set; }
		//----------------------------------------------------------------------
		public object Execute(Context context)
        {
            var url = (string) Url.Execute(context);   //given url
            //checking if conection is possible
            try
            {
                new HttpRequest().Get(url);
            }
            catch (Exception)
            {
                throw new Exception($"Unable to connect with {url}, check URL-address and your Internet connection!");
            }
            
            var allTags = new List<string>();   //all the found tags
            var tagName = (string)TagName.Execute(context);
            var tagNum = (int)TagNum.Execute(context);
            //parsing all the tags
            var parser = new HtmlParser();
            //write down all the code in the string
            var document = parser.Parse(new WebClient().DownloadString(url)); 
            //adding each tag
            foreach (var tag in document.QuerySelectorAll(tagName))
            {
                allTags.Add(tag.InnerHtml);
            }

            if (tagNum > allTags.Count)
                throw new Exception("Index was out of range: 'tagNum' in 'tag(url, tagName, tagNum)'.");

            return allTags[tagNum];
        }

        public TagExpression(IExpression url, IExpression tagName, IExpression tagNum)
        {
            Url = url;
            TagName = tagName;
            TagNum = tagNum;
        }
    }
    //--------------------------------------------------------------------------
    class TagExpressionCreator : IExpressionCreator
    {
        private SyntaxAnalyzer _analyzer;
        //----------------------------------------------------------------------
        public TagExpressionCreator(SyntaxAnalyzer analyzer)
        {
            _analyzer = analyzer;
        }
        //----------------------------------------------------------------------
        public IExpression CreateExpression(ref List<Token> tokens, ref int position)
        {
            //checking the type
            if (tokens[position].getType() != TokenType.Tag)
                return null;
            else
            {
                //checking the syntax 
                if (tokens[++position].getType() != TokenType.LeftParenthesis)
                    throw new Exception("Tag function syntax error: '(' is missing!");

                position++;
                var url = _analyzer.GetCurrentExpression(tokens, ref position);

                if(tokens[position].getType() != TokenType.Comma)
                    throw new Exception("Tag function syntax error: ',' is missing!");

                position++;
                var tagName = _analyzer.GetCurrentExpression(tokens, ref position);

                if (tokens[position].getType() != TokenType.Comma)
                    throw new Exception("Tag function syntax error: ',' is missing!");

                position++;
                var tagNum = _analyzer.GetCurrentExpression(tokens, ref position);

                if (tokens[position++].getType() != TokenType.RightParenthesis)
                    throw new Exception("Tag function syntax error: ')' is missing!");
                else
                    return new TagExpression(url, tagName, tagNum);
            }
        }
    }
}
//------------------------------------------------------------------------------