﻿//--------------------------------------------------------------------------
using System;
using System.Windows.Forms;
//--------------------------------------------------------------------------
namespace Interpretator
{
    //----------------------------------------------------------------------
    //main window of the program
    //----------------------------------------------------------------------
    public partial class MainWindow : Form
    {
        public MainWindow()
        {
            InitializeComponent();
        }
        //------------------------------------------------------------------
        private void MagicButton_Click(object sender, EventArgs e)
        {
            Console.Text = "";
            SyntaxAnalyzer.AnalyzerOutput = "";
            try
            {
                LexicAnalyzer lexer = new LexicAnalyzer();
                var tokens = lexer.Analyze(TextBox.Lines);
                
                SyntaxAnalyzer syntaxAnalyzer = new SyntaxAnalyzer();
                IExpression tree = syntaxAnalyzer.TreeBuilder(tokens);
				
				tree.Execute(new Context());

				Console.Clear();
                Console.Text += SyntaxAnalyzer.AnalyzerOutput;
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
            }
        }

		private void NotMagicButton_Click(object sender, EventArgs e)
		{
			Console.Clear();
			SyntaxAnalyzer.AnalyzerOutput = "";
			try
			{
				LexicAnalyzer lexer = new LexicAnalyzer();
				var tokens = lexer.Analyze(TextBox.Lines);

				foreach (var token in tokens)
				{
					Console.AppendText(token.ToString() + "\n");
				}

				SyntaxAnalyzer syntaxAnalyzer = new SyntaxAnalyzer();
				IExpression tree = syntaxAnalyzer.TreeBuilder(tokens);

				Console.AppendText(new TreePrinter().PrintTree(tree));
			}
			catch (Exception exc)
			{
				MessageBox.Show(exc.Message);
			}
		}

        private void Console_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
//--------------------------------------------------------------------------
