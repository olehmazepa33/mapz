﻿//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using xNet;
//------------------------------------------------------------------------------
namespace Interpretator
{
    class VisitFunction : IExpression
    {
        public IExpression Url { get; private set; }
		//----------------------------------------------------------------------
		public object Execute(Context context)
        {
            //trying to connect
            try
            {
                new HttpRequest().Get((string)Url.Execute(context));
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }
        //----------------------------------------------------------------------
        public VisitFunction(IExpression url)
        {
            Url = url;
        }
    }
    //--------------------------------------------------------------------------
    class VisitFunctionCreator : IExpressionCreator
    {
        private SyntaxAnalyzer _analyzer;
        //----------------------------------------------------------------------
        public VisitFunctionCreator(SyntaxAnalyzer analyzer)
        {
            _analyzer = analyzer;
        }
        //----------------------------------------------------------------------
        public IExpression CreateExpression(ref List<Token> tokens, ref int position)
        {
            //checking token's type
            if (tokens[position].getType() != TokenType.Visit)
                return null;
            //checking the syntax
            if (tokens[++position].getType() != TokenType.LeftParenthesis)
                throw new Exception($"Visit function syntax error: '(' is missing!");
            //getting the url
            position++;
            var url = _analyzer.GetCurrentExpression(tokens, ref position);

            if (tokens[position].getType() != TokenType.RightParenthesis)
                throw new Exception($"Visit function syntax error: ')' is missing!");
            else
            {
                position++;
                return new VisitFunction(url);
            }
        }
    }
}
//------------------------------------------------------------------------------