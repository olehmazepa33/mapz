﻿//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
//------------------------------------------------------------------------------
namespace Interpretator
{   
    class PrintFunction : IExpression
    {
        public IExpression InputData { get; private set; }
		//----------------------------------------------------------------------
		public PrintFunction(IExpression input)
        {
            InputData = input;
        }
        //----------------------------------------------------------------------
        public object Execute(Context context)
        {
            var arg = InputData.Execute(context);
            SyntaxAnalyzer.AnalyzerOutput += arg + "\n";

            return null;
        }
    }
    //--------------------------------------------------------------------------
    class PrintFunctionCreator : IExpressionCreator
    {
        private readonly SyntaxAnalyzer _analyzer;
        //----------------------------------------------------------------------
        public PrintFunctionCreator(SyntaxAnalyzer analyzer)
        {
            _analyzer = analyzer;
        }
        //----------------------------------------------------------------------
        public IExpression CreateExpression(ref List<Token> tokens, ref int position)
        {
            //checking token's type
            if (tokens[position].getType() != TokenType.Print)
                return null;
            //checking the syntax
            if (tokens[++position].getType() != TokenType.LeftParenthesis)
                throw new Exception("Print function syntax error: '(' is missing!");
            //expression to be printed
            position++;
            var data = _analyzer.GetCurrentExpression(tokens, ref position);

            if (tokens[position].getType() == TokenType.RightParenthesis)
            {
                position++;
                return new PrintFunction(data);
            }
            else throw new Exception("Print function syntax error: ')' is missing!");
        }
    }
}
//------------------------------------------------------------------------------