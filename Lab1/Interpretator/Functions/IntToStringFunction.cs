﻿//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
//------------------------------------------------------------------------------
namespace Interpretator
{
    //--------------------------------------------------------------------------
    //converts int to string
    //--------------------------------------------------------------------------
    class IntToStringFunction :IExpression
    {
        public IExpression Value { get; private set; }
		//----------------------------------------------------------------------
		public object Execute(Context context)
        {
            return ((int)Value.Execute(context)).ToString();
        }
        //----------------------------------------------------------------------
        public IntToStringFunction(IExpression value)
        {
            Value = value;
        }
    }
    //--------------------------------------------------------------------------
    class IntToStringCreator :IExpressionCreator
    {
        private SyntaxAnalyzer _analyzer;
        //----------------------------------------------------------------------
        public IntToStringCreator(SyntaxAnalyzer analyzer)
        {
            _analyzer = analyzer;
        }
        //----------------------------------------------------------------------
        public IExpression CreateExpression(ref List<Token> tokens, ref int position)
        {
            //checking token type
            if (tokens[position].getType() != TokenType.ToString)
                return null;
            //checking the syntax
            if (tokens[++position].getType() != TokenType.LeftParenthesis)
                throw new Exception("IntToString function syntax error: '(' is missing!");

            if (tokens[++position].getType() != TokenType.NumberConst)
                throw new Exception("IntToString function syntax error: int expected");
            var IntNum = _analyzer.GetCurrentExpression(tokens, ref position);

            if (tokens[position].getType() != TokenType.RightParenthesis)
                throw new Exception("IntToString function syntax error: ')' is missing!");
            else
            {
                position++;
                return new IntToStringFunction(IntNum);
            }
        }
    }
}
//------------------------------------------------------------------------------