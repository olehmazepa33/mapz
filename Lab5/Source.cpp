#include <iostream>
#include <vector>
using namespace std;



//������ �������
//���� ��� ��������� ���
class SGame
{
public:
	void Save()
	{
		cout << "Game saved" << endl;
	}
	void Restore()
	{
		cout << "Game restored" << endl;
	}
};
//���� ������� �������
class RecordTable
{
public:
	void SaveRecord()
	{
		cout << "Record Saved" << endl;
	}
};

//����������� ���� �������
class Command
{
public:
	virtual void DoCommand() = 0;
};
//������� ��� ���������� ���
class SaveGame :public Command
{
	SGame* sgame;
public:
	SaveGame(SGame* game)
	{
		sgame = game;
	}
	void DoCommand()
	{
		sgame->Save();
	}
};
//������� ��� ���������� ���
class RestoreGame :public Command
{
	SGame* sgame;
public:
	RestoreGame(SGame* game)
	{
		sgame = game;
	}
	void DoCommand()
	{
		sgame->Restore();
	}
};
//������� ��� ���������� �������
class SaveRecord :public Command
{
	RecordTable* RecTbl;
public:
	SaveRecord(RecordTable* RecTbl)
	{
		this->RecTbl = RecTbl;
	}
	void DoCommand()
	{
		RecTbl->SaveRecord();
	}
};
//���� ������, ���� ������� ������ �������
class Button
{
	Command* command;
public:
	Button(Command* command)
	{
		this->command = command;
	}
	void Click()
	{
		cout << "Click" << endl;
		command->DoCommand();
	}
};

class Ball;
//���� ���������� � ������� ��� ��������� ��������
class Observer
{
public:
	virtual void update(Ball* ball) = 0;
};
//���� �'����
class Ball
{
	bool isGetBonus;
	bool isAlive;
	vector<Observer*> Observers;
public:
	Ball()
	{
		isGetBonus = false;
		isAlive = true;
	}
	bool IsAlive()
	{
		return isAlive;
	}
	bool IsGetBonus()
	{
		return isGetBonus;
	}
	void collisionPlatform()
	{
		cout << "Ball bounced off the platform" << endl;
		isAlive = true;
		isGetBonus = false;
		Notify();
	}
	void collisionPlatformBonus()
	{
		cout << "Platform gets bonus" << endl;
		isGetBonus = true;
		isAlive = true;
		Notify();
	}
	void collisionWall()
	{
		cout << "Ball bounced off the wall" << endl;
		isGetBonus = false;
		isAlive = true;
		Notify();
	}
	void collisionBrick()
	{
		cout << "Ball bounced off the destroyed brick" << endl;
		isGetBonus = false;
		isAlive = true;
		Notify();
	}
	void collisionGround()
	{
		cout << "Ball is destroyed" << endl;
		isGetBonus = false;
		isAlive = false;
		Notify();
	}
	void addObserver(Observer* Obs)
	{
		Observers.push_back(Obs);
	}
	void Notify()
	{
		for (int i = 0; i < Observers.size(); i++)
		{
			Observers[i]->update(this);
		}
	}
};

//���������� ����������, ���� �'���� ����, ������� ���
class Game :public Observer
{
public:
	void update(Ball* ball)
	{
		if (!ball->IsAlive())
		{
			cout << endl << "Game stoped because the ball fell to the ground" << endl;
		}
	}
};
//���� �'���� ������ �����, �� ��������� ����� ���� ��������
class Behavior :public Observer
{
public:
	void update(Ball* ball)
	{
		if (ball->IsGetBonus())
		{
			cout << endl << "Behavior was change because ball get a bonus" << endl;
		}
	}
};




int main()
{
	SGame savegame;
	RecordTable RT;
	Button SaveGameButton(new SaveGame(&savegame));//������ ��� ���������� ���
	Button RestoreGameButton(new RestoreGame(&savegame));//������ ��� ���������� ���
	Button SaveRecButton(new SaveRecord(&RT));//������ ��� ���������� �������
	SaveGameButton.Click();
	RestoreGameButton.Click();
	SaveRecButton.Click();
	cout << endl << "-------------------------------------------" << endl << endl;
	//��������� �'���� � ������
	Ball ball;
	Game game;
	Behavior behavior;
	ball.addObserver(&game);
	ball.addObserver(&behavior);

	ball.collisionPlatform();
	ball.collisionWall();
	ball.collisionBrick();
	ball.collisionPlatformBonus();
	ball.collisionGround();
	cout << endl << "-------------------------------------------" << endl << endl;
}